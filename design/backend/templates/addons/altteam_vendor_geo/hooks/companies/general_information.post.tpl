{if $smarty.request.company_id}
    {include file="common/subheader.tpl" title=__("vendor_geo")}
    {assign var="destinations" value=fn_get_destinations()}
    {assign var="vendor_coverage" value=fn_get_vendor_coverage($smarty.request.company_id)}
    <div class="control-group store-locator__pickup-destinations-list">
    <label class="control-label">{__("warehouses.show_to")}{include file="common/tooltip.tpl" tooltip=__("warehouses.show_to_tooltip")}:</label>
    <div class="controls">
        <div class="destinations-wrapper">
            {* {$destinations|fn_print_r} *}
            {foreach $destinations as $destination}
                <label class="checkbox inline" for="destinations_{$destination.destination_id}">
                    <input
                            type="checkbox"
                            name="company_data[company_destinations_ids][]"
                            class="store-locator__destination"
                            id="destinations_{$destination.destination_id}"
                            {if $destination.destination_id|in_array:$vendor_coverage}
                                checked="checked"
                            {/if}
                            value="{$destination.destination_id}"
                    />{$destination.destination}
                </label>
            {/foreach}
        </div>
    </div>
    </div>
{/if}