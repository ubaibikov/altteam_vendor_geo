<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

namespace Tygh\VendorGeo;

class Manage
{
    protected $user_distention = 0;

    public function __construct($user_distention)
    {
        $this->user_distention = $user_distention;
    }

    public function getCurrentCompanyIds(): array
    {
        $companies = $this->getCompanies();
        return $companies;
    }

    protected function getCompanies(): array
    {
        $vendor_coverage = db_get_array("SELECT * FROM ?:vendor_coverage");
        $companies = [];

        foreach ($vendor_coverage as $vendor) {
            $destinations_ids = self::explodedArray($vendor['destinations_ids']);

            if (in_array($this->user_distention, $destinations_ids)) {
                $companies[] = $vendor['company_id'];
            }
        }

        return $companies;
    }

    public static function explodedArray(string $arr)
    {
        return explode(',', $arr);
    }
}
