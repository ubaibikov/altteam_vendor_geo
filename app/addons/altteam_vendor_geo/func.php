<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/


use Tygh\Registry;
use Tygh\Tygh;
use Tygh\Location\Manager;
use Tygh\VendorGeo\Manage;

/** 
 *  Hook handler: update company
 */
function fn_altteam_vendor_geo_update_company(&$company_data, &$company_id, $lang_code, $action)
{
    if (!empty($company_data['company_destinations_ids'])) {
        $company_main_id = db_get_field("SELECT company_id FROM ?:vendor_coverage WHERE company_id = ?i", $company_id);

        if (!empty($company_main_id)) {
            $coverge_data = [
                'destinations_ids' => implode(',', $company_data['company_destinations_ids']),
            ];

            db_query("UPDATE ?:vendor_coverage SET ?u WHERE company_id = ?i", $coverge_data, $company_id);
        } else {
            $coverge_data = [
                'company_id' => $company_id,
                'destinations_ids' => implode(',', $company_data['company_destinations_ids']),
            ];

            db_query("INSERT INTO ?:vendor_coverage SET ?u", $coverge_data);
        }
    } else {
        db_query("DELETE FROM ?:vendor_coverage WHERE company_id = ?i", $company_id);
    }
}

/** 
 *  Hook handler: delete company
 */
function fn_altteam_vendor_geo_delete_company(&$company_id, $result, $storefronts)
{
    $reset_vendor_geo = db_query("DELETE FROM ?:vendor_coverage WHERE company_id = ?i", $company_id);
}

function fn_get_vendor_coverage($company_id)
{
    $destinations_ids = db_get_field("SELECT destinations_ids FROM ?:vendor_coverage WHERE company_id = ?i", $company_id);
    return explode(',', $destinations_ids);
}

/** 
 *  Hook handler: changes the display of goods
 */
function fn_altteam_vendor_geo_get_products($params, $fields, $sortings, &$condition, $join, $sorting, $group_by, $lang_code, $having)
{
    if ($params['area'] == 'C') {
        $managing = fn_get_current_companies_ids();
        $condition .= db_quote(" AND products.company_id IN (?n)", $managing->getCurrentCompanyIds());
    }
}

/** 
 *  Hook handler: changes the display of companies
 */
function fn_altteam_vendor_geo_get_companies($params, $fields, $sortings, &$condition, $join, $auth, $lang_code, $group)
{
    if(AREA == 'C') {
        $managing = fn_get_current_companies_ids();
        $condition .= db_quote(" AND ?:companies.company_id IN (?n)", $managing->getCurrentCompanyIds());
    }
}

function fn_get_current_companies_ids(): Manage
{
    $manager = Tygh::$app['location'];
    $destination_id = $manager->getDestinationId();

    $managing = new Manage($destination_id);

    return $managing;
}